const sumar=require("../index");
const assert = require("assert");
//Assert=Afirmación
describe("Probar la suma de dos numeros",()=>{
    //Afirmamos q 5+5=10
    it("5+5=10",()=>{
        assert.equal(10,sumar(5,5));
    });
    //Afirmamos q 5+7!=10
    it("5+7!=10",()=>{
        assert.notEqual(10,sumar(5,7));
    });
});